/*****************************************************************************/
/*****************************************************************************/
/**************************GENERADOR DE PDFS**********************************/
/*****************************************************************************/
var PDFDocument = require('pdfkit');
/*construe un pdf de un recibo a partir de los datos de un movimiento
 */
var generatePdfMovement  = function ( user, iban, movement, res ) {
    console.log("generatePdfMovement - inicio");
    // create a document and pipe to a blob
    var doc = new PDFDocument();
    res.setHeader('Content-Disposition', 'attachment; filename="movement.pdf"');
    res.setHeader('Content-type', 'application/pdf');

    var stream = doc.pipe(res);

    // header
    doc.fontSize(10)
       .text('recibo de un movimiento de pyssabank', 10, 10);

    // Datos cliente / Cuenta
    doc.fontSize(15)
       .text('Cliente : '+user, 20, 60);
    doc.fontSize(15)
       .text('Cuenta : '+iban, 20, 80);

    //Datos del movimiento
    doc.fontSize(18)
       .text('Datos del movimiento:', 20, 120);
    doc.fontSize(10)
       .text('Fecha', 20, 150);
    doc.fontSize(10)
       .text('Importe', 250, 150);
    doc.fontSize(10)
       .text('Tipo', 500, 150);

    //Valores
    doc.fontSize(10)
       .text(movement.date, 20, 170);
    doc.fontSize(10)
       .text(movement.amount, 250, 170);
    doc.fontSize(10)
       .text(movement.type, 500, 170);

    // some vector graphics
    doc.save()
       .moveTo(10, 160)
       .lineTo(600, 160)
       .fill("#000000");


    // some vector graphics
    doc.save()
       .moveTo(5, 200)
       .lineTo(605, 200)
       .fill("#CCCCCC");

    doc.save()
       .moveTo(5, 5)
       .lineTo(605, 5)
       .fill("#CCCCCC");


    doc.save()
       .moveTo(5, 5)
       .lineTo(5, 200)
       .fill("#CCCCCC");

    doc.save()
       .moveTo(605, 5)
       .lineTo(605, 200)
       .fill("#CCCCCC");

    // end and display the document in the iframe to the right
    doc.end();
    stream.on('finish', function() {
      //iframe.src = stream.toBlobURL('application/pdf');
      console.log("generado pdf correctamente");
    });
}

module.exports.generatePdfMovement = generatePdfMovement
