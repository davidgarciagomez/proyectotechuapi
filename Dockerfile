#Imagen base la de node
FROM node
#Crea un directorio base donde ejecutar los comandos de docker
WORKDIR /apitechu
#Incorporo a la carpeta apitechu nuestro proyecto de node
ADD . /apitechu
#Exponemos al exterior el puesto de nuestra api
EXPOSE 3000
#Arrancamos nuestra api
CMD ["npm","start"]
