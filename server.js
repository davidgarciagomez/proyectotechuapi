  /****************************************/
/* DEFINICIÓN DE DEPENCENCIAS           */
/****************************************/
//Objeto que expone metodos para el envio de emails
var mail = require("./emailsender.js");
//Objeto que expone metodos de acceso a una bbdd mongo apificada
var bbdd = require('./rest-methods-mlab');
//Framework de desarollo web
var express = require('express');
//Libreria de trabajo con sistemas de ficheros.
var fs = require('fs');
//Objeto que manega la generación de PDFS
var pdf = require('./pdfgenerator');
// Inicializamos el framework expresss para utilizarlo
var app = express();
// Parseador del cuerpo de las peticiones para trabajar con json desde el
//cliente y nuestra api
var bodyParser = require('body-parser');
app.use(bodyParser.json());
// Por definicion establecemos que nuestra app escuche en el puerto 3000
var port = process.env.PORT || 3000;
// Arrancamos el framework de express
app.listen(port);
//Método que contiene metodos de validación de prametros.
var validador = require("./validation.js");
// Mostramos el fin de aranque
console.log("API Molona DGG&&ARL  escuchando en el puerto " + port);

/*****************************************************************************/
/****************************************/
/* MANEJADORES DE RUTAS DE ACCESO       */
/****************************************/
/*****************************************************************************/

/* Metodo que a partir de un id de usuario devuelve todos sus datos de la bbdd.
*/
app.get("/apitechu/v1/users/:id",
  function (req, res){
    console.log("GET /apitechu/v1/users/:id");
    var id = req.params.id;
    console.log("id : " +  id );
    var validacion = validador.isValidIdUser(id);
    if ( validacion.length > 0 ) {
      response = { "msg" : "La peticion no es correcta debido a que:\n" };
      for (var i = 0; i < validacion.length; i++ ){
        response.msg += "\n\t - " + validacion[i];
      }
      res.status(400);
      res.send(response);
    } else {
      //Peticion de obtencion de un usuario por id
      bbdd.get (bbdd.COLL_USER,'q={"id":' + id + '}',
        function (err, resMLab, body) {
            var response = {};
            if ( err ) {
              console.log("err:");
              console.log(err);
              response = { "msg" : "Error obteniendo el usuario con id :["  + id + "]." };
              res.status(500);
            } else if ( body.length == 0 ){
              response = { "msg" : "Usuario con id ["  + id + "] no encontrado."};
              res.status(404);
            } else if ( body.length > 1 ){
              response = { "msg" : "Usuario con id ["  + id + "] duplicado."};
              res.status(409);
            } else {
              response = body;
            }
            res.send(response);
        }
      );
    }
  }
);

/* Este metodo valida el correo y pwd pasados por parametro contra la bbdd
y si se encuentra coincidencia marca en la bbdd como logado al usuario, si
se produce cualquier error devolvera un codigo determinado.
*/
app.post("/apitechu/v1/login",
  function (req, res){
    console.log("POST /apitechu/v1/login");
    var email = req.body.email;
    console.log("email ["+email+"]");
    var password =  req.body.password;
    console.log("password ["+password+"]");
    var id = null;

    var validacion = validador.isValidLogin(email, password);
    if ( validacion.length > 0 ) {
      response = { "msg" : "La peticion no es correcta debido a que:\n" };
      for (var i = 0; i < validacion.length; i++ ){
        response.msg += "\n\t - " + validacion[i];
      }
      res.status(400);
      res.send(response);
    } else {
      //Peticion de obtencion de un usuario por id
      bbdd.get(bbdd.COLL_USER,'q={"email":"' + email + '"}',
        function (err, resMLab, body) {
          if ( err ){
            response = { "msg" : "Error realizando login del usuario :["  + email + "]." };
            res.status(500);
          } else if ( body.length == 1  ) {
            if ( body[0].password == password) {
              console.log("login correcto.");
              response = {"msg" : "login correcto", "idUsuario" : body[0].id, "first_name" : body[0].first_name, "last_name" : body[0].last_name };
              bbdd.put(bbdd.COLL_USER, 'q={"email":"' + email + '", "password":"'+password+'"}', '{"$set":{"logged":true}}',
                function(errPut, resPut, bodyPut){
                    if ( err ) {
                      console.log("login incorrecto - put");
                      response = { "msg" : "Error realizando login del usuario :["  + email + "]." };
                      res.status(500);
                    } else {
                      console.log("login correcto - put");
                    }
                    res.send(response);
                }
              );
            } else  {
              console.log("login incorrecto - pwd incorrecta");
              response = {"msg" : "Login incorrecto - Password incorrecta." };
              res.status(404);
              res.send(response);
            }
          } else if ( body.length > 1  ) {
            console.log("login incorrecto - mas de un usuario con ese email.");
            response = {"msg" : "Login incorrecto - mas de un usuario con ese email." };
            res.status(409);
            res.send(response);
          } else {
            console.log("login incorrecto - usuario no existe");
            response = {"msg" : "Login incorrecto - Usuario no existe." };
            res.status(404);
            res.send(response);
          }

        }
      );
    }
  }
);



/* Este metodo permite hacer el logout de un usuario logado por su id
   valida previamente que los datos sean correctos y persiste el cambio
   a logout en la bbdd.
*/
app.put("/apitechu/v1/logout",
  function (req, res){
    console.log("PUT /apitechu/v1/logout");
    var id = req.body.id;
    console.log("id ["+id+"]");

    var validacion = validador.isValidIdUser(id);
    if ( validacion.length > 0 ) {
      response = { "msg" : "La peticion no es correcta debido a que:\n" };
      for (var i = 0; i < validacion.length; i++ ){
        response.msg += "\n\t - " + validacion[i];
      }
      res.status(400);
      res.send(response);
    } else {
      //Peticion de obtencion de un usuario por id
      bbdd.get(bbdd.COLL_USER, 'q={"id":' + id + '}',
        function (err, resMLab, body) {
          if ( err ){
            response = { "msg" : "Error realizando logout del usuario con id:["  + id + "]." };
            res.status(500);
          } else if ( body.length == 1  ) {
            if ( body[0].logged ) {
              console.log("logout correcto");
              response = {"msg" : "logout correcto.", "idUsuario" : body[0].id };
              bbdd.put(bbdd.COLL_USER,
                       'q={"id":' + id + '}',
                       '{"$unset":{"logged":true}}',
                        function(errPut, resPut, bodyPut){
                            if ( err ) {
                              console.log("logout incorrecto - put");
                              response = { "msg" : "Error realizando logout del usuario con id :["  + id + "]." };
                              res.status(500);
                            } else {
                              console.log("logout correcto - put");
                            }
                            res.send(response);
                        }
              );
            } else {
              console.log("logout incorrecto - no logado");
              response = {"msg" : "logout incorrecto - usuario no logado."};
              res.status(404);
              res.send(response);
            }
          } else {
            console.log("logout incorrecto - usuario no existe");
            response = {"msg" : "logout incorrecto - usuario no existe."};
            res.status(404);
            res.send(response);
          }
        }
      );
    }
  }
);


/* Este metodo obtiene a partir de un id de usuario valido el listado de sus
cuentas asociadas
*/
app.get("/apitechu/v1/users/:id/accounts",
  function (req, res){
    console.log("GET /apitechu/v1/:id/accounts");
    var id = req.params.id;
    console.log("id ["+id+"]");
    var validacion = validador.isValidIdUser(id);
    if ( validacion.length > 0 ) {
      response = { "msg" : "La peticion no es correcta debido a que:\n" };
      for (var i = 0; i < validacion.length; i++ ){
        response.msg += "\n\t - " + validacion[i];
      }
      res.status(400);
      res.send(response);
    } else {
      //Peticion de obtencion de un usuario por id
      bbdd.get(bbdd.COLL_ACCOUNT,'q={"userid":' + id + '}&f={"movements":0}',
        function (err, resMLab, body) {
          console.log(body);
          if ( err ){
            response = { "msg" : "Error obteniendo las cuentas del usuario con id:["  + id + "]." };
            res.status(500);
          } else if ( body.length == 0 ){
            response = { "msg" : "El usuario con id:["  + id + "], no tiene cuentas asociadas." };
            res.status(404);
          } else {
              console.log("get accounts correcto.");
              console.log(body);
              response = body;
          }
          console.log(response);
          res.send(response);
        }
      );
    }
  }
);


/* Dado un iban este metodo devuelve toda la información de los movimientos
   asociados.
*/
app.get("/apitechu/v1/accounts/:iban/movements",
  function (req, res){
    console.log("GET /apitechu/v1/accounts/:iban/movements");
    var iban = req.params.iban;
    console.log("iban ["+iban+"]");
    var validacion = validador.isValidIban(iban);
    if ( validacion.length > 0 ) {
      response = { "msg" : "La peticion no es correcta debido a que:\n" };
      for (var i = 0; i < validacion.length; i++ ){
        response.msg += "\n\t - " + validacion[i];
      }
      res.status(400);
      res.send(response);
    } else {
      //Peticion de obtencion de un usuario por id
      bbdd.get(bbdd.COLL_ACCOUNT,'q={"iban" :"'+iban+'"}',
        function (err, resMLab, body) {
          console.log(body);
          if ( err ){
            response = { "msg" : "Error obteniendo los movimientos de la cuenta ["+iban+"]." };
            res.status(500);
          } else if ( body.length == 0 ){
            response = { "msg" : "La cuenta con iban :["  + iban + "], no existe." };
            res.status(404);
          } else if ( body.length == 0 ){
            response = { "msg" : "La cuenta con iban :["  + iban + "], esta duplicada." };
            res.status(409);
          } else {
              console.log("get movements correcto");
              console.log(body);
              response = body;
          }
          console.log(response);
          res.send(response);
        }
      );
    }
  }
);


/* Este metodo valida que la entrada por el body sea un movimiento valido
 y lo da de alta en la cuenta asociada.
*/
app.post("/apitechu/v1/accounts/:iban/movements",
  function (req, res){
    console.log("POST /apitechu/v1/accounts/:iban/movements");
    var iban = req.params.iban;
    console.log("iban ["+iban+"]");
    var type = req.body.type;
    console.log("type ["+type+"]");
    var amount = req.body.amount;
    console.log("amount ["+amount+"]");
    var date = req.body.date;
    console.log("date ["+date+"]");
    //Creacion del movimiento
    var newMovement = {
      "amount" : amount,
      "date" : date,
      "type"  : type,
      "idmovement" : 0
    };
    //Validamos que el movimiento sea correcto
    var validacion = validador.isValidMovement(newMovement);
    console.log(validacion);
    if ( validacion.length > 0){
      response = { "msg" : "La peticion no es correcta debido a que:\n" };
      for (var i = 0; i < validacion.length; i++ ){
        response.msg += "\n\t - " + validacion[i];
      }
      console.log(response);
      res.status(400);
      res.send(response);
    } else {
      newMovement.amount = parseFloat(newMovement.amount);
      bbdd.get(bbdd.COLL_ACCOUNT,'q={"iban" :"'+iban+'"}',
      function (err, resMLab, body) {
        if ( err ){
          response = { "msg" : "Error realizando el alta de un movimiento en la cuenta:["  + iban + "]." };
          res.status(500);
          res.send(response);
        } else if ( body.length == 0 ){
          response = { "msg" : "La cuenta con iban :["  + iban + "], no existe." };
          res.status(404);
          res.send(response);
        } else if ( body.length > 1 ){
          response = { "msg" : "La cuenta con iban :["  + iban + "], esta duplicada." };
          res.status(409);
          res.send(response);
        } else {
          //Peticion de actualización de los movimientos
          console.log("before:" + body[0].movements);
          //Establecemos el id unico dentro del listado de movimientos de la cuenta.
          var idMax = -1;
          for(var i = 0; i < body[0].movements.length; i++  ){
            if ( body[0].movements[i].id > idMax ){
              idMax = body[0].movements[i].id;
            }
          }
          newMovement.idmovement = idMax+1;
          body[0].movements.push(newMovement);
          console.log("after:" + body[0].movements);
          var query = 'q={"iban" :"'+iban+'"}';
          var set = '{"$set":{"movements":'+JSON.stringify(body[0].movements)+', "balance"  : '+(body[0].balance+newMovement.amount)+'}}';
          bbdd.put ( bbdd.COLL_ACCOUNT, query, set,
            function (err, resMLab, body) {
              if ( err ){
                response = { "msg" : "Error añadiendo un movimiento a la cuenta ["+iban+"]." };
                res.status(500);
              } else {
                  console.log("insert movements correcto.");
                  response = newMovement;
              }
              res.send(response);
            }
          );

        }
      });
    }
  }
);


/* Este metodo elimina un movimiento dentro del listado de una cuenta,
es obligatorio que se le pase un id de movimiento y la cuenta.
*/
app.delete("/apitechu/v1/accounts/:iban/movements/:idmovement",
  function (req, res){
    console.log("GET /apitechu/v1/accounts/:iban/movements");
    var iban = req.params.iban;
    console.log("iban ["+iban+"]");
    var idmovement = req.params.idmovement;
    console.log("idmovement ["+idmovement+"]");
    var movimientoBorrado = null;
    //Validamos que el movimiento sea correcto
    var validacion = validador.isValidDelMovement(iban, idmovement);
    console.log(validacion);
    if ( validacion.length > 0){
      response = { "msg" : "La peticion no es correcta debido a que:\n" };
      for (var i = 0; i < validacion.length; i++ ){
        response.msg += "\n\t - " + validacion[i];
      }
      res.status(400);
      res.send(response);
    } else {
        bbdd.get(bbdd.COLL_ACCOUNT,'q={"iban" :"'+iban+'"}',
        function (err, resMLab, body) {
          if ( err ){
            response = { "msg" : "Error realizando el borrado de un movimiento en la cuenta:["  + iban + "]." };
            res.status(500);
            res.send(response);
          } else if ( body.length == 0 ){
            response = { "msg" : "La cuenta con iban :["  + iban + "], no existe." };
            res.status(404);
            res.send(response);
          } else if ( body.length > 1 ){
            response = { "msg" : "La cuenta con iban :["  + iban + "], esta duplicada." };
            res.status(404);
            res.send(response);
          } else {
            //Peticion de borrado de los movimientos
            console.log("before:" + body[0].movements);
            //eliminamos el movimiento
            var amount = 0;
            var encontrado = false;
            for(var i = 0; i < body[0].movements.length; i++  ){
              if ( body[0].movements[i].id == idmovement ){
                movimientoBorrado = body[0].movements[i];
                amount = movimientoBorrado.amount;
                body[0].movements.splice(i,1);
                console.log(movimientoBorrado);
                encontrado = true;
                break;
              }
            }
            console.log("after:" + body[0].movements);
            if ( !encontrado ){
              response = { "msg" : "Movimiento con id ["+idmovement+"] no existe." };
              res.status(404);
              res.send(response);
            } else {
              var query = 'q={"iban" :"'+iban+'"}';
              var set = '{"$set":{"movements":'+JSON.stringify(body[0].movements)+', "balance"  : '+(body[0].balance+((-1)*amount))+'}}';
              bbdd.put ( bbdd.COLL_ACCOUNT, query, set,
                function (err, resMLab, body) {
                  if ( err ){
                    response = { "msg" : "Error eliminando un movimiento a la cuenta ["+iban+"]." };
                    res.status(500);
                  } else {
                      console.log("delete movements correcto.");
                      response = movimientoBorrado;
                  }
                  res.send(response);
                }
              );
            }
          }
        });
      }
  }
);



/* Da de alta una nueva cuenta para el usuario pasado por parametro.
*/
app.post("/apitechu/v1/user/:userId/accounts",
  function (req, res){
    var type = req.body.type;
    var alias = req.body.alias;
    var currency = req.body.currency;
    var userId = parseInt(req.params.userId);

    console.log("parametros : ["+type+"]["+alias+"]["+userId+"]");
    var newAccount = {
      "iban" : null,
      "type" : type,
      "balance" : 0.0,
      "movements" : [],
      "alias" : alias,
      "userid" : userId,
      "currency" : currency
    };
    //Validamos que sea una cuenta nueva valida.
    var validacion = validador.isValidAccount(newAccount);
    if ( validacion.length > 0){
      response = { "msg" : "La peticion no es correcta debido a que:\n" };
      for (var i = 0; i < validacion.length; i++ ){
        response.msg += "\n\t - " + validacion[i];
      }
      res.status(400);
      res.send(response);
    } else {
      bbdd.userExist(userId,
        function (err, resMLab, body) {
          console.log("body user exist.");
          console.log(body);
          if ( err ){
            response = { "msg" : "Error realizando el alta de una cuenta." };
            res.status(500);
            res.send(response);
          } else if ( body.length == 0 ){
            response = { "msg" : "El usuario no existe." };
            res.status(404);
            res.send(response);
          } else if ( body.length > 1 ){
            response = { "msg" : "Usuario esta duplicado." };
            res.status(404);
            res.send(response);
          } else {
              bbdd.getNewIban(
                function(err, newIban){
                console.log(newIban);
                if (err){
                  response = { "msg" : "Error realizando el alta de una cuenta nueva." };
                  res.status(500);
                  res.send(response);
                } else {
                  bbdd.getNewAccountId(
                    function(err, newId){
                        newAccount.iban = newIban;
                        newAccount.id = newId;
                        console.log(newAccount);
                        bbdd.post(bbdd.COLL_ACCOUNT,newAccount,
                         function (err, resMLab, body) {
                           if (err){
                             response = { "msg" : "Error realizando el alta de una cuenta nueva." };
                             res.status(500);
                             res.send(response);
                           } else {
                             res.send(body);
                           }
                         });
                    });
                  }
              });
            }
      });
    }
  });

/* Valida y da de alta un usuario en la bbdd
*/
app.post("/apitechu/v1/user",
  function (req, res){
    console.log("POST /apitechu/v1/user");
    var newUser = {
        "id": null,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "address": req.body.address,
        "country": req.body.country,
        "postalcode": req.body.postalcode,
        "password": req.body.password,
        "avatar": req.body.avatar,
        "phone": req.body.phone,
        "birthday": req.body.birthday,
        "documentid": req.body.documentid
      };
    //Validamos que sea una cuenta nueva valida.
    var validacion = validador.isValidUser(newUser);

    if ( validacion.length > 0){
      response = { "msg" : "La peticion no es correcta debido a que:\n" };
      for (var i = 0; i < validacion.length; i++ ){
        response.msg += "\n\t - " + validacion[i];
      }
      res.status(400);
      res.send(response);
    } else {
      console.log("validando email existente");
      bbdd.emailExist(newUser.email,
        function (err, resMLab, body) {
          console.log("body email exist");
          console.log(body);
          if ( err ){
            console.log(err);
            response = { "msg" : "Error realizando el alta de un usuario." };
            res.status(500);
            res.send(response);
          } else if ( body.length > 0 ){
            response = { "msg" : "El email YA existe." };
            res.status(400);
            res.send(response);
          } else {
            bbdd.getNewUserId(
              function(err, newUserId){
                console.log("newUserId");
                if (err){
                  response = { "msg" : "Error dando de alta de un nuevo usuario." };
                  res.status(500);
                  res.send(response);
                } else {
                newUser.id = newUserId;
                console.log(newUser);
                bbdd.post(bbdd.COLL_USER,newUser,
                 function (err, resMLab, body) {
                   console.log("post alta");
                   if (err){
                     response = { "msg" : "Error dando de alta de un usuario nuevo." };
                     res.status(500);
                     res.send(response);
                   } else {
                     res.send(body);
                   }
                 }
                );
              }
              });
          }
      });
    }
  });


  /********* SEND MAIL */
  /* Este metodo permite utilizando una api externa
     enviar un email para recordar la contraseña de un usuasrio dado de alta
     o bien avisar al front de que el usuario no existe o esta dado incorrectamente
     de alta en la bbdd, ya que existen mas de una cuenta con ese email.*/
  app.post("/apitechu/v1/sendmailpwd/:email",
    function (req, res){
      console.log("post /apitechu/v1/sendmailpwd/:email");
        var email = req.params.email;
        console.log("Parametro.email " + email);
        //Validamos que sea una cuenta nueva valida.
        var validacion = validador.isValidEmail(email);

        if ( validacion.length > 0){
          response = { "msg" : "La peticion no es correcta debido a que:\n" };
          for (var i = 0; i < validacion.length; i++ ){
            response.msg += "\n\t - " + validacion[i];
          }
          res.status(400);
          res.send(response);
        } else {
        /*Obtenemos los datos del usuario para transmitir la pwd*/
            bbdd.get (bbdd.COLL_USER,'q={"email":"' + email + '"}',
            function (err, resMLab, body) {
              console.log("body:");
              console.log(body);
              if (err){
                console.log("error en la peticion get");
                console.log(err);
                response = { "msg" : "Error recuperando la contraseña del usuario." };
                res.status(500);
                res.send(response);
              } else if ( body.length == 0  ) {
                console.log("no se ha encontrado el usuario");
                response = { "msg" : "No existe el usuario." };
                res.status(404);
                res.send(response);
              } else if ( body.length > 1 ) {
                console.log("mas de un usuario con esa cuenta de email");
                response = { "msg" : "Mas de un usuario con esa cuenta." };
                res.status(404);
                res.send(response);
              } else {
                var user = body[0];
                /*Cambiamos su contraseña a su numeor de telefono*/
                user.password = user.phone;
                console.log("datos obtenidos correctos, enviamos el email de recuperacion.");
                bbdd.put(bbdd.COLL_USER,'q={"id":' + user.id + '}', JSON.stringify(user),
                function (err, resMLab, body) {
                  if (err){
                    console.log("update error");
                    console.log(err);
                    response = { "msg" : "Error actualizando la contraseña del usuario." };
                    res.status(500);
                    res.send(response);
                  } else {
                    mail.sendEmailPwd(user.email,
                    function (err, resMLab, body) {
                      if (err){
                        console.log("sendemailpwd error");
                        console.log(err);
                        response = { "msg" : "Error recuperando la contraseña del usuario." };
                        res.status(500);
                        res.send(response);
                      } else {
                            response = { "msg" : "Contraseña enviada al email." };
                            res.send(response);
                          }
                      });
                  }
                });
                  }
                });
              }
        });

/*  Generador de un movimiento en formato pdf como recibo.
 *  Recibe los datos a mostrar a traves del cuerpo post, en lugar
 *  de ir a la bbdd, ya que siempre se va a llamar en un punto en el
 *  que se tiene toda la información, de este modo hacemos mas eficiente
 *  el metodo.

 movementpdf = {"user" : "david garcia gomez",
                "iban" : "ES 45 0182 9512 1234567980",
                "movement" : { "id" : 2, "amount" : -251248.12, "type" : "reintegro", "date" : "2018/05/05" }}
 */
app.post("/apitechu/v1/accounts/movements/pdf",
  function (req, res){
    console.log("POST /apitechu/v1/accounts/:iban/movements/:idmovement/pdf");
    var movementpdf = req.body;
    console.log("movementpdf");
    console.log(movementpdf);

    pdf.generatePdfMovement( movementpdf.user,
                             movementpdf.iban,
                             movementpdf.movement,
                             res );
});
