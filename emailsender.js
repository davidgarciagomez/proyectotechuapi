/*****************************************************************************/
/*****************************************************************************/
/**************************REST API   ****************************************/
/*****************************************************************************/
//Base de la url de peticiones a EMAULSENDER.
const sendGridBaseURL = "https://api.sendgrid.com";
/* Api key  para el consumo*/
const apikey = "Bearer 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";
/*Librería de tratamiento de llamadas json*/
var requestJson =  require("request-json");

var httpClientSend = requestJson.createClient(sendGridBaseURL);

var body_email_pwd = {
  "personalizations": [{"to": null}],
  "from": {"email": "support@pyssabank.es"},
  "subject": "PyssaBank - Recuperar contraseña olvidada",
  "content": [{"type": "text/plain", "value": null}]
}

httpClientSend.headers["Authorization"] = apikey;
httpClientSend.headers["Content-Type"] = "application/json";

/*
 * Metodo que haciendo uso de una api, envia un email para recordar la pwd al
 * usuario.
 */
var sendEmailPwd  = function sendEmailPwd(email, callback){
  body_email_pwd.personalizations[0].to =  [{"email": email}];
  body_email_pwd.content[0].value =  "Tu contraseña pasa a ser tu número de teléfono, no olvides cambiarla al entrar.";
  console.log("body_email_pwd:");
  console.log(body_email_pwd);
  console.log("httpClientSend");
  console.log(httpClientSend);
  httpClientSend.post("/v3/mail/send",body_email_pwd,callback);
}

//METODOS Y CONSTANTES VISIBLES

module.exports.sendEmailPwd = sendEmailPwd;
