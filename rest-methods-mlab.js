
/*****************************************************************************/
/*****************************************************************************/
/**************************REST API   ****************************************/
/*****************************************************************************/

/* Este objeto permite aislar al server de la api de bbdd utilizada*/

/**REQUIRE*/
var formato = require("format-number");
var miFormatoIban = formato({padLeft: 10});
var requestJson =  require("request-json");

/* CONSTANTES*/
const COLL_USER = "user";
const COLL_ACCOUNT = "account";
const IBAN_AUTOGENERADO = "ES14 0182 9999 00 ";
const SEQUENCE_ID = "sequenceiban";

//Base de la url de peticiones a MLAB.
const mLabBaseURL = "https://api.mlab.com/api/1/databases/proyectotechu/collections/";
/* Api key de mlab para el consumo de */
const mLabAPIKey = "apiKey=w_woORSGTtk09y9gDPPzXM_oVrtwEGJB";
//Creamos el cliente rest
var httpClient = requestJson.createClient(mLabBaseURL);



/*
 * Creamos un metodo manejador de la llamada a la api mlab GET
 */
var get = function get (collection, query, callback){
    console.log("method get");
    console.log("url ["+collection + "?" + query + "&" + mLabAPIKey+"]");
    httpClient.get(collection + "?" + query + "&" + mLabAPIKey,callback);
};

/*
 * Creamos un metodo manejador de la llamada a la api mlab DEL
 */
var del = function del (collection, query, callback){
    console.log("method del");
    console.log("url ["+collection + "?" + query + "&" + mLabAPIKey+"]");
    httpClient.delete(collection + "?" + query + "&" + mLabAPIKey,callback);
};


/*
 * Creamos un metodo manejador de la llamada a la api mlab PUT
 */
var put =  function put (collection, query, body, callback) {
  console.log("method put");
  console.log("url ["+collection + "?" + query + "&" + mLabAPIKey+"]");
  httpClient.put(collection + "?" + query + "&" + mLabAPIKey, JSON.parse(body),callback);
};


/*
 * Creamos un metodo manejador de la llamada a la api mlab POST
 */
var post =  function post (collection, body, callback) {
  console.log("method post");
  console.log("url ["+collection + "?" + mLabAPIKey+"]");
  httpClient.post(collection + "?" + mLabAPIKey, body,callback);
};

/*
 * Obtiene el siguiente iban a ser utilizado para un alta.
 * Lo obtiene a partir de un documento en la colección de cuentas, que almacena
 * El ultimo id utilizdo, al que luego se le prefija el formato de iban.
*/
var getNewIban = function( callback ){
  console.log("getNewIban");
  httpClient.get(COLL_ACCOUNT + '?' + 'q={"_id" : "'+SEQUENCE_ID+'" }&f={"sequence_value":1}&' + mLabAPIKey,
  function (err, resMLab, body) {
    if ( err ){
      console.log("error generando el nuevo iban" + err);
      callback(err, iban);
    } else {
      console.log(body[0]);
      var nextVal = body[0].sequence_value;
      httpClient.put(COLL_ACCOUNT + '?q={"_id" : "'+SEQUENCE_ID+'" }&' + mLabAPIKey,
                    {"$set":{ "sequence_value" : (body[0].sequence_value+1) }},
                    function(err, resMLab, body){
                      console.log(body);
                      if ( err ){
                        console.log("error generando el nuevo iban" + err);
                        callback(err, iban);
                      } else {
                        var iban =  IBAN_AUTOGENERADO + miFormatoIban(nextVal,{ noSeparator: true});
                        console.log("new iban "+iban);
                        callback(err, iban);
                      }
                    }
                  );
    }
  });
}

/*
 * Obtiene el siguiente id de usuario a ser utilizado en un alta, se Obtiene
 * a partir del ultimo creado.
 */
var getNewUserId = function ( callback ){
  console.log("getNewUserId");
  console.log("url :" + COLL_USER + '?f={"id":1}&l=1&s={"id":-1}&' + mLabAPIKey);
  httpClient.get(COLL_USER + '?f={"id":1}&l=1&s={"id":-1}&' + mLabAPIKey,
  function (err, resMLab, body) {
    var nextVal = 0;
    console.log(body);
    if ( err ){
      console.log("error generando el nuevo userid" + err);
      callback(err, -1);
    } else if ( body.length > 0 ) {
      nextVal = body[0].id + 1;
    }
    callback(err, nextVal);
  });
}

/*
 * Obtiene el siguiente id de cuenta a ser utilizado en un alta, se Obtiene
 * a partir del ultimo creado.
 */
var getNewAccountId = function ( callback ){
  console.log("getNewAccountId");
  console.log("url :" + COLL_ACCOUNT + '?f={"id":1}&l=1&s={"id":-1}&' + mLabAPIKey);
  httpClient.get(COLL_ACCOUNT + '?f={"id":1}&l=1&s={"id":-1}&' + mLabAPIKey,
  function (err, resMLab, body) {
    var nextVal = 0;
    console.log(body);
    if ( err ){
      console.log("error generando el nuevo accountid" + err);
      callback(err, -1);
    } else if ( body.length > 0 ) {
      nextVal = body[0].id + 1;
    }
    callback(err, nextVal);
  });
}

/*
 * Comprueba si un usuario ya existe en la colección de usuario a partir
 * del id pasado, es util para validar en el alta de una cuenta que lo
 * asociamos correctamente a un usuario existente.
 */
var userExist = function ( userid, callback ){
  console.log("userExist");
  httpClient.get(COLL_USER + '?q={"id" : '+userid+'}&' + mLabAPIKey,callback);
}

/*
 * Comprueba si un email ya esta dado de alta para no duplicar emails a la
 * hora de dar de alta usuarios.
 */
var emailExist = function ( email, callback ){
  console.log("emailExist");
  httpClient.get(COLL_USER + '?q={"email" : "'+email+'"}&' + mLabAPIKey,callback);
}

//METODOS Y CONSTANTES VISIBLES

module.exports.get = get;
module.exports.put = put;
module.exports.post = post;
module.exports.delete = del;


module.exports.getNewIban = getNewIban;
module.exports.getNewUserId = getNewUserId;
module.exports.getNewAccountId = getNewAccountId;
module.exports.userExist = userExist;
module.exports.emailExist = emailExist;

module.exports.COLL_USER = COLL_USER;
module.exports.COLL_ACCOUNT = COLL_ACCOUNT;
