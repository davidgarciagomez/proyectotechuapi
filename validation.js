/*****************************************************************************/
/*****************************************************************************/
/**************************VALIDATION ****************************************/
/*****************************************************************************/
//Libreria de validaciones
var validator = require('validator');
//Validacion de fecha
var rePatternDate = new RegExp(/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/);

//Valida que sea un movimiento correcto para ser insertado en la bbdd.
var isValidMovement = function ( movement ) {
  console.log("isValidMovement");
  var resultado = new Array();
    if ( isNull(movement.type) || !validator.isIn(movement.type, ["reintegro","ingreso"])) {
      resultado.push('El tipo no es correcto ["reintegro","ingreso"]');
    }
    if ( isNull(movement.date) || !rePatternDate.test(movement.date) ){
      resultado.push("La fecha no tiene un formato correcto  YYYY/MM/DD.");
    }
  console.log(resultado);
  return resultado;
}

//Valida que sea una cuenta correcta para ser insertada en la bbdd.
var isValidAccount = function (newAccount){
  console.log("isValidAccount");
  var resultado = new Array();
  if ( isNull(newAccount.type) || !validator.isIn(newAccount.type, ["ahorro","vista","corriente"]) ){
    resultado.push('Tipo de cuenta no valido ["ahorro","vista","corriente"].');
  }
  if ( isNull(newAccount.userid) || !validator.isInt(newAccount.userid+"", {"min" : 0}) ){
    resultado.push("No es un identificador de usuario valido.");
  }
  console.log(resultado);
  return resultado;
}

//Valida que sea un usuario correcto para ser insertado en la bbdd.
var isValidUser  = function (newUser){
  console.log("isValidUser");
  var resultado = new Array();
  if ( isNull(newUser.first_name) || validator.isEmpty(newUser.first_name)  ) {
    resultado.push("El campo 'first_name' no puede ser vacio.");
  }
  if (isNull(newUser.last_name) || validator.isEmpty(newUser.last_name)  ) {
    resultado.push("El campo 'last_name' no puede ser vacio.");
  }
  if (isNull(newUser.email) || !validator.isEmail(newUser.email)  ) {
    resultado.push("El campo 'email' no es correcto.");
  }
  if ( isNull(newUser.address) || validator.isEmpty(newUser.address)  ) {
    resultado.push("El campo 'address' no puede ser vacio.");
  }
  if ( isNull(newUser.country) || validator.isEmpty(newUser.country)  ) {
    resultado.push("El campo 'country' no puede ser vacio.");
  }
  if ( isNull(newUser.password) || validator.isEmpty(newUser.password)  ) {
    resultado.push("El campo 'password' no puede ser vacio.");
  }
  if ( isNull(newUser.password) || newUser.password.length < 8 ) {
    resultado.push("El campo 'password' debe tener una longitud minima de 8 caracteres, y contener letras, numeros y caracteres especiales.");
  }
  console.log(resultado);
  return resultado;
}

//Valida que sea un identificador de usuario correcto.
var isValidIdUser =  function ( iduser ) {
  console.log("isValidIdUser");
  var resultado = new Array();
  if ( isNull(iduser) || !validator.isInt(iduser+'',{"min":0})){
    resultado.push("No es un identificador de usuario valido.");
  }
 console.log(resultado);
 return resultado;
}


//Valida que sea un email de usuario correcto.
var isValidEmail =  function ( email ) {
  console.log("isValidEmail");
  var resultado = new Array();
  if ( isNull(email) || !validator.isEmail(email)){
    resultado.push("No es un email de usuario valido.");
  }
 console.log(resultado);
 return resultado;
}




//Valida que los datos de email y password tengan un formato correcto para realziar el login.
var isValidLogin = function (email, password) {
  console.log("isValidLogin");
  var resultado = new Array();
  if (  isNull(email) || !validator.isEmail(email)){
    resultado.push("No es un email correcto.");
  }
  if ( isNull(password) || validator.isEmpty(password)){
    resultado.push("La contraseña esta vacía.");
  }
  console.log(resultado);
  return resultado;
}

//Valida que sea un iban correcto para realizar consultas.
var isValidIban = function  (iban) {
  console.log("isValidIban");
  var resultado = new Array();
  if (  isNull(iban) ||  validator.isEmpty(iban.trim())){
    resultado.push("El iban esta vacío.");
  }
  console.log(resultado);
  return resultado;
}

//Valida que tengamos un iban y un id correctos para realizar el borrado de un
//movimiento
var isValidDelMovement = function  (iban, id) {
  console.log("isValidDelMovement");
  var resultado = new Array();
  if (  isNull(iban) ||  validator.isEmpty(iban.trim())){
    resultado.push("El iban esta vacío.");
  }
  if (  isNull(id) || !validator.isInt(id+"",{"min" : 0})  ) {
    resultado.push("Identificador de movimiento invalido.");
  }
  console.log(resultado);
  return resultado;
}


function isNull ( obj ){
  return (typeof obj == 'undefined' || obj == null);
}

//METODOS Y CONSTANTES VISIBLES

module.exports.isValidMovement = isValidMovement;
module.exports.isValidAccount = isValidAccount;
module.exports.isValidUser = isValidUser;
module.exports.isValidIdUser = isValidIdUser;
module.exports.isValidLogin = isValidLogin;
module.exports.isValidIban = isValidIban;
module.exports.isValidDelMovement = isValidDelMovement
module.exports.isValidEmail = isValidEmail
